import { Component, OnInit } from '@angular/core';
import { PrecoBitcoin } from '../models/PrecoBitcoin';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    public precoBitcoin : PrecoBitcoin = new PrecoBitcoin();

    constructor(private service: DashboardService){}

    ngOnInit(): void {
      this.service.get()
        .subscribe({
          next: (response) => {this.precoBitcoin = response},
          error: (error) => console.log(error),
          
        })
    }
}
