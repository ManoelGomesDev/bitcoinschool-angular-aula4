import { Component } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {
  constructor(){}
  public valorBitcoin: number = 0;
  public criptomoeda: string = "";

  addBitcoin(){
    this.valorBitcoin++;
  }

  removeBitcoin(){
    this.valorBitcoin--;
  }

  delBitcoin(){
    this.valorBitcoin=0;
  }

  KeyUp(event: any){
    this.criptomoeda = event.target.value;
  }

}
